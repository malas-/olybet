<?php

namespace Tests\Unit;

use App\Http\Requests\Bet;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Support\Facades\Validator;
use Tests\TestCase;

class BetRequestValidatorTest extends TestCase
{

	use WithFaker;

    /**
     * Empty request should fail.
     *
     * @return void
     */
    public function testEmptyTest()
    {
    	$validator = Validator::make([], Bet::rules());
        $this->assertTrue($validator->fails());
    }

    /**
     * Empty request should fail.
     *
     * @return void
     */
    public function testValidTest()
    {
    	$data = $this->generateRequestData();
    	$validator = Validator::make($data, Bet::rules());
        $this->assertFalse($validator->fails());
    }

    /**
     * Empty request should fail.
     *
     * @return void
     */
    public function testEmptySelectionsTest()
    {
    	$data = $this->generateRequestData();
    	$data['selections'] = [];
    	$validator = Validator::make($data, Bet::rules());
        $this->assertFalse($validator->fails());
    }

    /**
     * Fully valid Bet request.
     *
     */
    private function generateRequestData() {
    	$data = [];
    	$data['player_id'] = $this->faker->randomDigitNotNull();
    	$data['stake_amount'] = ''.$this->faker->randomFloat(2, 1, 150);
    	$data['selections'] = $this->generateSelections(3);
    	return $data;
    }

    private function generateSelections($count) {
    	$data = [];
    	for ($i = 0; $i <= $count; $i++) {
    		$data[] = [
	    		'id' => $this->faker->randomDigitNotNull(),
	    		'odds' => ''.$this->faker->randomFloat(2, 1.01, 22.5)
	    	];
    	}
    	return $data;
    }
}
