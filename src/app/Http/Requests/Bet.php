<?php

namespace App\Http\Requests;

class Bet
{

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    static public function rules()
    {
        return [
            'player_id' => 'required|integer',
            'stake_amount' => 'required|string',
            'selections' => 'required|array',
            'selections.*.id' => 'required|integer',
            'selections.*.odds' => 'required|string'
        ];
    }
}
