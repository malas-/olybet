<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Http\Requests\Bet;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class BetController extends Controller
{
    /**
     * User can make a bet.
     *
     * @param  json  $data BetData encoded as JSON
     * @return View
     */
    public function __invoke(Request $request)
    {
        $data = $request->input('data');

        $validator = Validator::make($request->all(), Bet::rules());

        if ($validator->fails()) {
            return response()->json(['errors'], 200, [], JSON_FORCE_OBJECT);
        }

        return response()->json([], 200, [], JSON_FORCE_OBJECT);
    }
}